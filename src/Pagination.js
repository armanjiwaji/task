import React, { useState, useEffect } from "react"


const Pagination = ({ showPerPage, onPaginationChange, total }) => {
    
    const [counter, setCounter] = useState(1);

    useEffect(() => {
        const value = showPerPage * counter;
        onPaginationChange(value - showPerPage, value);
    }, [counter]);

    const onButtonClick = (type) => {
        if (type === "prev") {
            if (counter === 1) {
                setCounter(1);
            } else {
                setCounter(counter - 1);
            }
        } else if (type === "next") {
            if (Math.ceil(total / showPerPage) === counter) {
                setCounter(counter);
            } else {
                setCounter(counter + 1);
            }
        }
    };
    return (
        <div style={{textAlign: 'center', marginTop: '20px'}}>
            <button
                style={{ marginRight: '20px' }}
                className="ui primary button"
                onClick={() => onButtonClick("prev")}
            >
                Prev
            </button>
            <button
                className="ui primary button"
                onClick={() => onButtonClick("next")}
            >
                Next
            </button>
        </div>

    );
};

export default Pagination