import React, { useState, useEffect } from "react";
import { Form } from "semantic-ui-react";
import "./App.css";
import Pagination from "./Pagination";
import axios from "axios";


const App = () => {


  //====================Sort===========================//
  const sorting = [
    { value: "1", text: "a-z" },
    { value: "2", text: "z-a" },
  ];

  const handleChange = (e, data) => {
    console.log(data.value);
    var value = data.value;
    if (value === "1") {
      var q = posts.sort(compareValues("name", "asc"));
      setPosts([...posts, q]);
    } else {
      var p = posts.sort(compareValues("name", "desc"));
      setPosts([...posts, p]);
    }
  };

  function compareValues(key, order = "asc") {
    return function innerSort(a, b) {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        return 0;
      }
      const varA = typeof a[key] === "string" ? a[key].toUpperCase() : a[key];
      const varB = typeof b[key] === "string" ? b[key].toUpperCase() : b[key];

      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return order === "desc" ? comparison * -1 : comparison;
    };
  }

  //===============PAGINATION==================//

  const [showPerPage, setPerPage] = useState(4);
  const [pagination, setPagination] = useState({ start: 0, end: showPerPage });
  const onPaginationChange = (start, end) => {
    setPagination({ start: start, end: end });
  };

  //================Search-Filter===========================//

  const [searchText, setSearchText] = useState("");

  //====================================================//

  const [posts, setPosts] = useState([]);

  useEffect(() => {
    async function getData() {
      const res = await axios
        .get("https://swapi.dev/api/people/")
        .then((res) => {
          setPosts(res.data.results);
          // console.log(res.data);
          // console.log(res.data.results);
        })
        .catch((err) => {
          console.log(err);
        });
    }
    getData();
  }, []);


  // useEffect(() => {
  //     async function data() {
  //         const resp = await axios.get('https://swapi.dev/api/species/2/')
  //         console.log("species data", resp)
  //     }
  //     data();
  // })

  //===========================//

  return (
    <div className="App">
      <br />

      <Form>
        <Form.Group>
          <Form.Input
            placeholder="Filter by Name..."
            value={searchText}
            onChange={(e) => setSearchText(e.target.value)}
          />
          <Form.Select
            placeholder="---sort by name---"
            fluid
            selection
            options={sorting}
            onChange={handleChange}
          />
        </Form.Group>
      </Form>

      {/* <i class="fab fa-android"></i>
            <i class="fas fa-question"></i>
            <i class="fas fa-user-circle"></i> */}

      <table>
        <thead>
          <tr className="head-name">
            <td>Name</td>
            <td>Height</td>
            <td>Mass</td>
            <td>Icon</td>
          </tr>
        </thead>
        <tbody>
          {
            posts.filter((item) => {if (searchText === "") {
                return item;
              } else if (item.name.toLowerCase().includes(searchText.toLowerCase())) {
                   return item;
              }
             })
             .slice(pagination.start, pagination.end)
             .map((item, i) => {
              return (
                <tr key={i}>
                  <td>{item.name}</td>
                  <td>{item.height}</td>
                  <td>{item.mass}</td>
                  <td></td>
                </tr>
              );
             })
            }
        </tbody>
      </table>
      <Pagination
        showPerPage={showPerPage}
        onPaginationChange={onPaginationChange}
        total={posts.length}
        setPerPage={setPerPage}
      />
      <br />
      <br />
    </div>
  );
};
export default App;